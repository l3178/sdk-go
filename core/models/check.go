package core_models

type CheckResponse struct {
	LicenseResponse
	InstallationFileResponse

	LicenseActive  bool `json:"license_active,omitempty"`
	LicenseEnabled bool `json:"license_enabled,omitempty"`
	IsExpired      bool `json:"is_expired,omitempty"`

	DeviceLicensed bool `json:"is_device_licensed,omitempty"`
	IsClockTampered  bool `json:"is_clock_tampered,omitempty"`
	ProductMismatch bool `json:"product_mismatch,omitempty"`
	FloatingExpired bool `json:"floating_expired,omitempty"`
}

type LocalCheckResponse struct {
	LicenseResponse

	LicenseActive  bool `json:"license_active"`
	LicenseEnabled bool `json:"license_enabled"`
	IsExpired      bool `json:"is_expired"`

	DeviceLicensed bool `json:"is_device_licensed"`
	IsClockTampered  bool `json:"is_clock_tampered"`
	ProductMismatch bool `json:"product_mismatch"`
	FloatingExpired bool `json:"floating_expired"`
}
