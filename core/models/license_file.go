package core_models

import (
	"time"
)


type LicenseFile struct {
	LicenseResponse LicenseResponse
	LastUpdatedTime time.Time   `json:"last_updated,omitempty"`
	LicenseEnabled bool `json:"license_enabled"`
	FloatingInUse bool `json:"floating_in_use"`
}