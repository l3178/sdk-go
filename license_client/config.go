package license_client

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"time"
	"gitlab.com/l3178/sdk-go/license_file"
	"github.com/denisbrodbeck/machineid"
	"gitlab.com/l3178/sdk-go/core/configuration"
)

const (
	baseUrl   = "https://api.licensespring.com"
	apiPrefix = "/api/v4/"

	signaturePrefix = "licenseSpring"
	headers         = "date"
	algorithm       = "hmac-sha256"
	dateFormat      = time.RFC1123
)

type LicenseClientConfiguration struct {
	*configuration.CoreConfiguration
	CryptoProvider license_file.CryptoProvider
	DataLocation license_file.DataLocation

	ApiKey    string
	SharedKey string

	ServerPublicKey string

	ProductCode string
	HardwareId  string

	AppName    string
	AppVersion string

	VerifySignature bool
}

func (config LicenseClientConfiguration) AuthHeader(date time.Time) string {
	dateStr := date.Format(dateFormat)
	signatureString := fmt.Sprintf("%s\ndate: %s", signaturePrefix, dateStr)

	encoder := hmac.New(sha256.New, []byte(config.SharedKey))
	encoder.Write([]byte(signatureString))
	encodedSignature := base64.StdEncoding.EncodeToString(encoder.Sum(nil))

	return fmt.Sprintf("algorithm=\"%s\",headers=\"%s\",signature=\"%s\",apikey=\"%s\"",
		algorithm, headers, encodedSignature, config.ApiKey)
}

func (config LicenseClientConfiguration) OfflineLicenseSignature(date time.Time, licenseKey string) string {
	dateStr := date.Format(dateFormat)
	signatureString := fmt.Sprintf("%s\ndate: %s\n%s\n%s\n%s",
		signaturePrefix, dateStr, licenseKey, config.HardwareId, config.ApiKey)

	encoder := hmac.New(sha256.New, []byte(config.SharedKey))
	encoder.Write([]byte(signatureString))
	return base64.StdEncoding.EncodeToString(encoder.Sum(nil))
}

func (config LicenseClientConfiguration) UrlPrefix() string {
	return config.CoreConfiguration.UrlPrefix()
}

func NewLicenseClientConfiguration(apiKey, sharedKey, productCode string) LicenseClientConfiguration {
	config := configuration.NewClientConfig(baseUrl, apiPrefix)

	hardwareId, _ := machineid.ProtectedID(configuration.SdkName)

	return LicenseClientConfiguration{
		CryptoProvider: license_file.DefaultCryptoProvider{},
		DataLocation: license_file.DefaultDataLocation{},
		CoreConfiguration: config,
		ApiKey:            apiKey,
		SharedKey:         sharedKey,
		ProductCode:       productCode,
		HardwareId:        hardwareId,
		VerifySignature:   true,
	}
}

func NewLicenseClientConfigurationCustom(cryptoProvider license_file.CryptoProvider, dataLocation license_file.DataLocation, apiKey, sharedKey, productCode string) LicenseClientConfiguration {
	config := configuration.NewClientConfig(baseUrl, apiPrefix)

	hardwareId, _ := machineid.ProtectedID(configuration.SdkName)

	return LicenseClientConfiguration{
		CryptoProvider: cryptoProvider,
		DataLocation: dataLocation,
		CoreConfiguration: config,
		ApiKey:            apiKey,
		SharedKey:         sharedKey,
		ProductCode:       productCode,
		HardwareId:        hardwareId,
		VerifySignature:   true,
	}
}
