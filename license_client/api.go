package license_client

import (
	"context"
	"fmt"
	"encoding/json"
	"time"
	"encoding/base64"

	"github.com/go-resty/resty/v2"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"gitlab.com/l3178/sdk-go/core/auth"
	"gitlab.com/l3178/sdk-go/core/client"
	. "gitlab.com/l3178/sdk-go/core/models"
	"gitlab.com/l3178/sdk-go/license_file"
)

const (
	signatureHeader = "LicenseSignature"
)

func (c *LicenseClient) ActivateLicense(ctx context.Context, request ActivationRequest) client.Response[LicenseResponse] {
	request.Product = c.ProductCode
	request.HardwareId = c.HardwareId

	body, err := c.c.Post(ctx, "activate_license", nil, request)
	if err != nil {
		return client.ErrorResponse[LicenseResponse](err)
	}

	if err := c.verifySignature(body); err != nil {
		return client.ErrorResponse[LicenseResponse](err)
	}

	license_response := client.NewResponse[LicenseResponse](body, nil)
	license_file.SaveLicense(c.CryptoProvider, c.DataLocation, license_response.Value, c.HardwareId, c.SharedKey)
	return license_response
}

func (c *LicenseClient) DeactivateLicense(ctx context.Context, request LicenseRequest) error {
	request.Product = c.ProductCode
	request.HardwareId = c.HardwareId
	_, err := c.c.Post(ctx, "deactivate_license", nil, request)
	return err
}

func (c *LicenseClient) CheckLicense(ctx context.Context, request ActivationRequest) client.Response[CheckResponse] {
	request.Product = c.ProductCode
	request.HardwareId = c.HardwareId
	body, err := c.c.Get(ctx, "check_license", nil, request)
	if c.verifySignature(body) != nil {
		return client.ErrorResponse[CheckResponse](err)
	}
	return client.NewResponse[CheckResponse](body, err)
}


// GenerateOfflineLicenseRequest takes ActivationRequest object as input and generates a new OfflineRequest.
// This function is responsible for setting up the various fields like HardwareId, ApiKey, Date, Request, 
// Signature, RequestId in an OfflineRequest.
func (c *LicenseClient) GenerateOfflineLicenseRequest(request ActivationRequest, requestType OfflineRequestType) client.Response[OfflineRequest] {

	providedData := request.LicenseRequest

	date := time.Now()
	apiKey := c.ApiKey
	hardwareId := c.HardwareId
	providedData.HardwareId = hardwareId
	signingKey := providedData.Key
	if providedData.Username != "" {
		signingKey = providedData.Username
	}

	activationReq := ActivationRequest{
		LicenseRequest: providedData,
	}

	generatedRequest := OfflineRequest{
		ApiKey:                 apiKey,
		Date:                   date.Format(time.RFC1123),
		Request:                requestType,
		Signature: c.OfflineLicenseSignature(date, signingKey),
		RequestId:              uuid.New().String(),
		ActivationRequest: activationReq,
	}


	payload, err := json.Marshal(&generatedRequest)

	return client.Response[OfflineRequest]{
		Payload: payload,
		Value:   generatedRequest,
		Error:   err,
	}
}

func (c *LicenseClient) ActivateOffline(ctx context.Context, request OfflineRequest) client.Response[LicenseResponse] {
	if request.Request != OfflineActivationRequest {
		err := errors.New("Activate offline request needs to be of activate type")
		return client.ErrorResponse[LicenseResponse](err)
	}
	request.Product = c.ProductCode
	request.HardwareId = c.HardwareId

	payload, _ := json.Marshal(&request)
	encodedPayload := base64.StdEncoding.EncodeToString(payload)

	body, err := c.c.PostPlainText(ctx, "activate_offline", nil, encodedPayload)
	if c.verifySignatureOffline(body) != nil {
		return client.ErrorResponse[LicenseResponse](err)
	}

	license_response := client.NewResponse[LicenseResponse](body, err)
	license_file.SaveLicense(c.CryptoProvider, c.DataLocation, license_response.Value, c.HardwareId, c.SharedKey)
	return license_response
}

func (c *LicenseClient) DeactivateOffline(ctx context.Context, request OfflineRequest) error {
	if request.Request != OfflineDeactivationRequest {
		return errors.New("Deactivate offline request needs to be of deactivate type")
	}

	request.Product = c.ProductCode
	request.HardwareId = c.HardwareId

	payload, _ := json.Marshal(&request)
	encodedPayload := base64.StdEncoding.EncodeToString(payload)

	_, err := c.c.PostPlainText(ctx, "deactivate_offline", nil, encodedPayload)
	return err

}

func (c *LicenseClient) AddConsumption(ctx context.Context, request ConsumptionRequest) client.Response[ConsumptionResponse] {
	request.Product = c.ProductCode
	request.HardwareId = c.HardwareId
	body, err := c.c.Post(ctx, "add_consumption", nil, request)
	return client.NewResponse[ConsumptionResponse](body, err)
}

func (c *LicenseClient) AddFeatureConsumption(ctx context.Context, request FeatureConsumptionRequest) client.Response[FeatureConsumptionResponse] {
	request.Product = c.ProductCode
	request.HardwareId = c.HardwareId
	body, err := c.c.Post(ctx, "add_feature_consumption", nil, request)
	return client.NewResponse[FeatureConsumptionResponse](body, err)
}

func (c *LicenseClient) TrialKey(ctx context.Context, request TrialLicenseRequest) client.Response[TrialKeyResponse] {
	request.Product = c.ProductCode
	request.HardwareId = c.HardwareId
	body, err := c.c.Get(ctx, "trial_key", nil, request)
	return client.NewResponse[TrialKeyResponse](body, err)
}

func (c *LicenseClient) ProductDetails(ctx context.Context) client.Response[ProductDetails] {
	request := ProductDetailsRequest{Product: c.ProductCode}
	body, err := c.c.Get(ctx, "product_details", nil, request)
	return client.NewResponse[ProductDetails](body, err)
}

func (c *LicenseClient) TrackDeviceVariables(ctx context.Context, request DeviceVariablesRequest) error {
	request.Product = c.ProductCode
	request.HardwareId = c.HardwareId
	_, err := c.c.Post(ctx, "track_device_variables", nil, request)
	return err
}

func (c *LicenseClient) GetDeviceVariables(ctx context.Context, request LicenseRequest) client.Response[[]DeviceVariable] {
	request.Product = c.ProductCode
	request.HardwareId = c.HardwareId
	body, err := c.c.Get(ctx, "get_device_variables", nil, request)
	return client.NewResponse[[]DeviceVariable](body, err)
}

func (c *LicenseClient) FloatingBorrow(ctx context.Context, request FloatingBorrowRequest) client.Response[FloatingBorrowResponse] {
	request.Product = c.ProductCode
	request.HardwareId = c.HardwareId
	body, err := c.c.Post(ctx, "floating/borrow", nil, request)
	return client.NewResponse[FloatingBorrowResponse](body, err)
}

func (c *LicenseClient) FloatingRelease(ctx context.Context, request LicenseRequest) error {
	request.Product = c.ProductCode
	request.HardwareId = c.HardwareId
	_, err := c.c.Post(ctx, "floating/release", nil, request)
	return err
}

func (c *LicenseClient) ChangePassword(ctx context.Context, request ChangePasswordRequest) error {
	_, err := c.c.Post(ctx, "change_password", nil, request)
	return err
}

func (c *LicenseClient) Versions(ctx context.Context, request LicenseRequest) client.Response[[]Version] {
	request.Product = c.ProductCode
	request.HardwareId = c.HardwareId
	body, err := c.c.Get(ctx, "versions", nil, request)
	return client.NewResponse[[]Version](body, err)
}

func (c *LicenseClient) InstallationFile(ctx context.Context, request LicenseRequest) client.Response[InstallationFileResponse] {
	request.Product = c.ProductCode
	request.HardwareId = c.HardwareId
	body, err := c.c.Get(ctx, "installation_file", nil, request)
	return client.NewResponse[InstallationFileResponse](body, err)
}

func (c *LicenseClient) CustomerLicenseUsers(ctx context.Context, request CustomerLicenseUsersRequest) client.Response[CustomerLicenseUsersResponse] {
	request.Product = c.ProductCode
	body, err := c.c.Get(ctx, "customer_license_users", nil, request)
	return client.NewResponse[CustomerLicenseUsersResponse](body, err)
}

func (c *LicenseClient) SSOUrl(ctx context.Context, request SSOUrlRequest) client.Response[SSOUrlResponse] {
	request.Product = c.ProductCode
	body, err := c.c.Get(ctx, "sso_url", nil, request)
	return client.NewResponse[SSOUrlResponse](body, err)
}

func (c *LicenseClient) verifySignature(resp *resty.Response) error {

	if resp == nil {
		return fmt.Errorf("response is nil")
	}

	if !c.VerifySignature {
		return nil
	}

	signature := resp.Header().Get(signatureHeader)
	if signature == "" {
		return fmt.Errorf("signature header not found")
	}

	if c.ServerPublicKey == "" {
		return auth.VerifySignatureV2(signature, string(resp.Body()))
	}

	return auth.VerifySignatureV2WithPubKey(c.ServerPublicKey, signature, string(resp.Body()))
}


func (c *LicenseClient) verifySignatureOffline(resp *resty.Response) error {

	if resp == nil {
		return fmt.Errorf("response is nil")
	}

	if !c.VerifySignature {
		return nil
	}

	signature := resp.Header().Get(signatureHeader)
	if signature == "" {
		return fmt.Errorf("signature header not found")
	}

	if c.ServerPublicKey == "" {
		return auth.VerifySignatureOfflineV2(signature, resp.Body())
	}

	return auth.VerifySignatureOfflineV2WithPubKey(c.ServerPublicKey, signature, resp.Body())
}


func (c *LicenseClient) LocalLicenseCheck(ctx context.Context, request LocalLicenseCheckRequest) client.Response[LocalCheckResponse] {

	if request.Product != c.ProductCode {
		return client.ErrorResponse[LocalCheckResponse](fmt.Errorf("Product does not match"))
	}

	lcs_file, err := license_file.LoadLicense(c.CryptoProvider, c.DataLocation, request.Product, c.SharedKey)
	if err != nil {
		return client.ErrorResponse[LocalCheckResponse](err)
	}

	product_mismatch := (lcs_file.LicenseResponse.ProductDetails.ShortCode != c.ProductCode)

	local_check_response := LocalCheckResponse{
		LicenseResponse: lcs_file.LicenseResponse,
		LicenseActive: lcs_file.LicenseResponse.Active,
		IsExpired: license_file.IsLicenseExpired(lcs_file),
		IsClockTampered: license_file.IsClockTampered(lcs_file),
		DeviceLicensed: c.HardwareId == lcs_file.LicenseResponse.HardwareId,
		ProductMismatch: product_mismatch,
		FloatingExpired: license_file.IsFloatingExpired(lcs_file),
		LicenseEnabled: lcs_file.LicenseEnabled,
	}

	return client.NewLocalResponse[LocalCheckResponse](local_check_response, nil)
}