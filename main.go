package main

import (
	"gitlab.com/l3178/sdk-go/samples"
)

func main() {

	//--- offline activation and deactivation ---//
	samples.ExampleOfflineActivation("your-offline-product-code")
	samples.ExampleOfflineDeactivation("your-offline-product-code")
	//--- online activation and deactivation --- //
	samples.ExampleOnlineActivation("your-online-product-code")
	samples.ExampleOnlineDeactivation("your-online-product-code")
	//--- offline and online activation with local license check ---//
	samples.ExampleOnlineActivationAndLocalCheck("your-online-product-code")
	samples.ExampleOfflineActivationAndLocalCheck("your-offline-product-code")
}