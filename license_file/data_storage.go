package license_file

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

// StoreEncryptedData writes any data to a file in JSON format
func StoreEncryptedData(filePath string, data interface{}) error {
	// Convert the data to JSON
	jsonData, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("failed to convert data to JSON: %v", err)
	}

	// Write the JSON data to the specified file
	err = ioutil.WriteFile(filePath, jsonData, 0600) // 0600 permissions for security
	if err != nil {
		return fmt.Errorf("failed to write encrypted data to file: %v", err)
	}

	return nil
}

// LoadEncryptedData reads data from a file and unmarshals it into the provided output interface
func LoadEncryptedData(filePath string, output interface{}) error {
	// Read the JSON data from the file
	jsonData, err := ioutil.ReadFile(filePath)
	if err != nil {
		return fmt.Errorf("failed to read encrypted data from file: %v", err)
	}

	// Unmarshal the JSON data into the output interface
	err = json.Unmarshal(jsonData, output)
	if err != nil {
		return fmt.Errorf("failed to unmarshal JSON data: %v", err)
	}

	return nil
}
