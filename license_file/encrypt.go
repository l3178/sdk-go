package license_file

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io"
	"encoding/json"

	"gitlab.com/l3178/sdk-go/core/models"
	"golang.org/x/crypto/pbkdf2"
)

// Constants for PBKDF2
const (
	iterationCount = 100000 // Number of PBKDF2 iterations
	keyLength      = 32     // AES-256 requires a 32-byte key
	saltSize       = 16     // Salt size in bytes
	ivSize         = aes.BlockSize // IV size in bytes (AES block size is 16 bytes)
)

type EncryptedData struct {
	Salt       string // Base64-encoded salt
	IV         string // Base64-encoded initialization vector
	Ciphertext string // Base64-encoded ciphertext
}

type DefaultCryptoProvider struct{}

// Encrypt method for AES-256-CBC encryption
func (d DefaultCryptoProvider) Encrypt(license_file core_models.LicenseFile, password string) (interface{}, error) {

	// generate the string plaintext
	license_json, err := json.Marshal(license_file)
	if err != nil {
		return nil, fmt.Errorf("failed to convert license file object to json: %v", err)
	}
	plaintext := string(license_json)

	// Step 1: Generate a random salt
	salt := make([]byte, saltSize)
	if _, err := io.ReadFull(rand.Reader, salt); err != nil {
		return nil, fmt.Errorf("failed to generate salt: %v", err)
	}

	// Step 2: Derive the encryption key using PBKDF2-HMAC-SHA256
	key := pbkdf2.Key([]byte(password), salt, iterationCount, keyLength, sha256.New)

	// Step 3: Generate a random IV
	iv := make([]byte, ivSize)
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return nil, fmt.Errorf("failed to generate IV: %v", err)
	}

	// Step 4: Encrypt the plaintext using AES-256-CBC
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, fmt.Errorf("failed to create AES cipher: %v", err)
	}

	paddedData := pad([]byte(plaintext), aes.BlockSize)
	ciphertext := make([]byte, len(paddedData))
	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(ciphertext, paddedData)

	// Step 5: Encode salt, IV, and ciphertext in base64 and return as EncryptedData struct
	return EncryptedData{
		Salt:       base64.StdEncoding.EncodeToString(salt),
		IV:         base64.StdEncoding.EncodeToString(iv),
		Ciphertext: base64.StdEncoding.EncodeToString(ciphertext),
	}, nil
}

// Decrypt method for AES-256-CBC decryption
func (d DefaultCryptoProvider) Decrypt(data interface{}, password string) (interface{}, error) {
	// Type assertion to ensure data is of type EncryptedData
	encryptedData, ok := data.(EncryptedData)
	if !ok {
		return nil, fmt.Errorf("invalid data type for decryption")
	}

	// Step 1: Decode the base64-encoded salt, IV, and ciphertext
	salt, err := base64.StdEncoding.DecodeString(encryptedData.Salt)
	if err != nil {
		return nil, fmt.Errorf("failed to decode salt: %v", err)
	}

	iv, err := base64.StdEncoding.DecodeString(encryptedData.IV)
	if err != nil {
		return nil, fmt.Errorf("failed to decode IV: %v", err)
	}

	ciphertext, err := base64.StdEncoding.DecodeString(encryptedData.Ciphertext)
	if err != nil {
		return nil, fmt.Errorf("failed to decode ciphertext: %v", err)
	}

	// Step 2: Derive the decryption key using PBKDF2-HMAC-SHA256
	key := pbkdf2.Key([]byte(password), salt, iterationCount, keyLength, sha256.New)

	// Step 3: Decrypt the ciphertext using AES-256-CBC
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, fmt.Errorf("failed to create AES cipher: %v", err)
	}

	if len(ciphertext)%aes.BlockSize != 0 {
		return nil, fmt.Errorf("ciphertext is not a multiple of the block size")
	}

	mode := cipher.NewCBCDecrypter(block, iv)
	plaintext := make([]byte, len(ciphertext))
	mode.CryptBlocks(plaintext, ciphertext)

	// Step 4: Unpad the decrypted data
	plaintext, err = unpad(plaintext, aes.BlockSize)
	if err != nil {
		return nil, fmt.Errorf("failed to unpad plaintext: %v", err)
	}

	return string(plaintext), nil
}

// Pads the data to be a multiple of the block size using PKCS#7 padding
func pad(data []byte, blockSize int) []byte {
	padding := blockSize - len(data)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(data, padtext...)
}

// Unpads the data using PKCS#7 padding
func unpad(data []byte, blockSize int) ([]byte, error) {
	length := len(data)
	if length == 0 || length%blockSize != 0 {
		return nil, fmt.Errorf("data is not properly padded")
	}
	padding := int(data[length-1])
	if padding > blockSize || padding == 0 {
		return nil, fmt.Errorf("invalid padding")
	}
	for _, v := range data[length-padding:] {
		if int(v) != padding {
			return nil, fmt.Errorf("invalid padding")
		}
	}
	return data[:length-padding], nil
}

