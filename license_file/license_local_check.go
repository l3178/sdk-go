package license_file

import (
	"time"
	"gitlab.com/l3178/sdk-go/core/models"
)

func IsClockTampered(lcs_file core_models.LicenseFile) bool {

	if time.Now().UTC().Before(lcs_file.LastUpdatedTime) {
		return true
	}
	return false
}

func IsLicenseExpired(lcs_file core_models.LicenseFile) bool {
	
	license_expiry_date := lcs_file.LicenseResponse.ValidityPeriod 

	if license_expiry_date.IsZero() {
		return false
	}

	if time.Now().UTC().After(lcs_file.LicenseResponse.ValidityPeriod) {
		return true
	}
	return false
}

func IsFloatingExpired(lcs_file core_models.LicenseFile) bool {

	license_response := lcs_file.LicenseResponse
	lastUpdatedUTC := lcs_file.LastUpdatedTime.UTC()

	if !(license_response.IsFloating || license_response.IsFloatingCloud) {
		return false
	}

	floating_expiry := lastUpdatedUTC.Add(time.Duration(license_response.FloatingTimeout) * time.Minute)

	if time.Now().UTC().After(floating_expiry) {
		return true
	}
	return false
}