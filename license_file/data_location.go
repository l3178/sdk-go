package license_file

import (
	"fmt"
	"path/filepath"
	"runtime"
	"os"
)

type DataLocation interface {
	DataDirectory() (string, error)
	LicenseFileName(string) string
}

type DefaultDataLocation struct{}

func (d DefaultDataLocation) DataDirectory() (string, error) {
	switch runtime.GOOS {
	case "windows":
		systemDrive := os.Getenv("SystemDrive")
		if systemDrive == "" {
			return "", fmt.Errorf("could not determine system drive on Windows")
		}
		userName := os.Getenv("USERNAME")
		if userName == "" {
			return "", fmt.Errorf("could not determine user name on Windows")
		}
		return filepath.Join(systemDrive, "Users", userName, "AppData", "Local", "LicenseSpring"), nil

	case "linux":
		homeDir := os.Getenv("HOME")
		if homeDir == "" {
			return "", fmt.Errorf("could not determine home directory on Linux")
		}
		return filepath.Join(homeDir, ".LicenseSpring", "LicenseSpring"), nil

	case "darwin":
		homeDir := os.Getenv("HOME")
		if homeDir == "" {
			return "", fmt.Errorf("could not determine home directory on macOS")
		}
		return filepath.Join(homeDir, "Library", "Application Support", "LicenseSpring"), nil

	default:
		return "", fmt.Errorf("unsupported operating system: %s", runtime.GOOS)
	}
}

func (d DefaultDataLocation) LicenseFileName(productCode string) string {
	return productCode + ".json"
}
