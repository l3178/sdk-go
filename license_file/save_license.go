package license_file

import (
	"time"
	"fmt"
	"path/filepath"
	"gitlab.com/l3178/sdk-go/core/models"
	"encoding/json"
	"os"
)

func SaveLicense(d CryptoProvider, data_location DataLocation, license_response core_models.LicenseResponse, hardwareId, password string) (error) {

	license_file := core_models.LicenseFile{
		LicenseResponse: license_response,
		LastUpdatedTime: time.Now().UTC(),
		LicenseEnabled: true,
	}

	data_dir, err := data_location.DataDirectory()
	CreateDirectory(data_dir)

	if err != nil {
		return fmt.Errorf("failed to find default directory: %v. Define custom directory.", err)
	}
	productCode := license_response.ProductDetails.ShortCode
	file_path := filepath.Join(data_dir, data_location.LicenseFileName(productCode))

	encryptedData, err := d.Encrypt(license_file, password)
	if err != nil {
		return fmt.Errorf("Error encrypting JSON: %v", err)
	}

	data, ok := encryptedData.(interface{})
	if !ok {
		return fmt.Errorf("Error: unexpected data type from Encrypt")
	}

	err = StoreEncryptedData(file_path, data)
	if err != nil {
		return fmt.Errorf("Error storing encrypted data: %v", err)
	}

	fmt.Println("License successfully stored at", file_path)

	return nil
}


func LoadLicense(d CryptoProvider, data_location DataLocation, productCode, password string) (core_models.LicenseFile, error) {

	data_dir, err := data_location.DataDirectory()
	if err != nil {
		return core_models.LicenseFile{}, fmt.Errorf("failed to find default directory: %v. Define custom directory.", err)
	}
	file_path := filepath.Join(data_dir, data_location.LicenseFileName(productCode))

	// Load encrypted data from file
	var encryptedData EncryptedData
	err = LoadEncryptedData(file_path, &encryptedData)
	if err != nil {
		return core_models.LicenseFile{}, fmt.Errorf("failed to load encrypted data: %v", err)
	}

	// Decrypt the license data
	decryptedData, err := d.Decrypt(encryptedData, password)
	if err != nil {
		return core_models.LicenseFile{}, fmt.Errorf("failed to decrypt data: %v", err)
	}

	// Unmarshal the decrypted data back into LicenseFile struct
	var licenseFile core_models.LicenseFile
	err = json.Unmarshal([]byte(decryptedData.(string)), &licenseFile)
	if err != nil {
		return licenseFile, fmt.Errorf("failed to unmarshal decrypted data: %v", err)
	}

	return licenseFile, nil

}

func CreateDirectory(location string) error {

	if _, err := os.Stat(location); err == nil {
		return nil
	} else if !os.IsNotExist(err) {
		return fmt.Errorf("error checking directory: %w", err)
	}

	err := os.MkdirAll(location, os.ModePerm)
	if err != nil {
		return fmt.Errorf("failed to create directory %s: %w", location, err)
	}

	return nil
}