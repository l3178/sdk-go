package license_file

import (
	"gitlab.com/l3178/sdk-go/core/models"
)

// CryptoProvider interface for custom encryption implementations
type CryptoProvider interface {
	Encrypt(license_file core_models.LicenseFile, password string) (interface{}, error)
	Decrypt(data interface{}, password string) (interface{}, error)
}
