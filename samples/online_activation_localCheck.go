package samples

import (
	"context"
	"fmt"

	"gitlab.com/l3178/sdk-go/core/auth"

	core_request "gitlab.com/l3178/sdk-go/core/models/request"
	"gitlab.com/l3178/sdk-go/license_client"
)


func ExampleOnlineActivationAndLocalCheck(productCode string) error {
	config, err := ReadConfig()
	if err != nil {
		return err
	}

	productAuth, exists := config.Products[productCode]
	if !exists {
		return fmt.Errorf("product code '%s' not found in configuration", productCode)
	}

	clientConfig := license_client.NewLicenseClientConfiguration(
		config.ApiKey,
		config.SharedKey,
		productCode,
	)
	clientConfig.Verbose = config.Verbose
	clientConfig.VerifySignature = config.VerifySignature

	client := license_client.NewLicenseClient(clientConfig)


	resp := client.ActivateLicense(context.Background(), license_client.ActivationRequest{
		LicenseRequest: core_request.LicenseRequest{
			Product: productCode,
			Auth: auth.FromKey(productAuth.LicenseKey),
			// Auth: auth.FromUsername(productAuth.Username, productAuth.Password),
		},
	})
	fmt.Println(resp.Error)
	fmt.Println(string(resp.Payload))


	resp2 := client.LocalLicenseCheck(context.Background(), license_client.LocalLicenseCheckRequest{Product: productCode})
	fmt.Println("Local Check Error: ", resp2.Error)
	fmt.Println("Local Check Payload: ", string(resp2.Payload))
	fmt.Println("Local Check Value: ", resp2.Value)

	return nil


}

