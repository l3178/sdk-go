package samples

import (
	"fmt"
	"os"
	"path/filepath"
	"gopkg.in/yaml.v2"
)

type ProductAuth struct {
	Username   string `yaml:"username,omitempty" json:"username,omitempty"`
	Password   string `yaml:"password,omitempty" json:"password,omitempty"`
	LicenseKey string `yaml:"licenseKey,omitempty" json:"licenseKey,omitempty"`
}

type Config struct {
	ApiKey       string            				`yaml:"apiKey,omitempty"`
	SharedKey   string           				`yaml:"sharedKey,omitempty"`
	Verbose        bool            			  	`yaml:"verbose,omitempty"`
	VerifySignature bool             			`yaml:"verify_signature,omitempty"`
	Products            map[string]ProductAuth 	`yaml:"products,omitempty"`
}


func ReadConfig() (*Config, error) {
	wd, err := os.Getwd()
	if err != nil {
		return nil, fmt.Errorf("failed to get working directory: %w", err)
	}

	configPath := filepath.Join(wd, "samples", "config.yaml")
	data, err := os.ReadFile(configPath)
	if err != nil {
		return nil, fmt.Errorf("failed to read config file: %w", err)
	}

	var config Config
	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, fmt.Errorf("failed to parse config: %w", err)
	}

	return &config, nil
}

