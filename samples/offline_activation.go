package samples

import (
	"context"
	"fmt"

	"gitlab.com/l3178/sdk-go/core/auth"
	core_request "gitlab.com/l3178/sdk-go/core/models/request"
	"gitlab.com/l3178/sdk-go/license_client"
)


// ExampleOfflineActivation demonstrates the offline activation.
func ExampleOfflineActivation(productCode string) error {
	config, err := ReadConfig()
	if err != nil {
		return err
	}

	// Get the ProductAuth details for the given product code.
	productAuth, exists := config.Products[productCode]
	if !exists {
		return fmt.Errorf("product code '%s' not found in configuration", productCode)
	}

	clientConfig := license_client.NewLicenseClientConfiguration(
		config.ApiKey,
		config.SharedKey,
		productCode,
	)
	clientConfig.Verbose = config.Verbose
	clientConfig.VerifySignature = config.VerifySignature

	client := license_client.NewLicenseClient(clientConfig)

	activationRequest := license_client.ActivationRequest{
		LicenseRequest: core_request.LicenseRequest{
			Product: productCode,
			Auth:    auth.FromKey(productAuth.LicenseKey),
		},
	}

	offlineReq := client.GenerateOfflineLicenseRequest(activationRequest, license_client.OfflineActivationRequest)

	ctx := context.Background()
	resp := client.ActivateOffline(ctx, offlineReq.Value)

	fmt.Println("Error:", resp.Error)
	fmt.Println("Payload:", string(resp.Payload))

	return nil
}

