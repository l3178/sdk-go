package samples

import (
	"context"
	"fmt"
	"gitlab.com/l3178/sdk-go/core/auth"
	core_request "gitlab.com/l3178/sdk-go/core/models/request"
	"gitlab.com/l3178/sdk-go/license_client"
)


func ExampleOnlineDeactivation(productCode string) error {
	config, err := ReadConfig()
	if err != nil {
		return err
	}

	productAuth, exists := config.Products[productCode]
	if !exists {
		return fmt.Errorf("product code '%s' not found in configuration", productCode)
	}

	clientConfig := license_client.NewLicenseClientConfiguration(
		config.ApiKey,
		config.SharedKey,
		productCode,
	)
	clientConfig.Verbose = config.Verbose
	clientConfig.VerifySignature = config.VerifySignature

	client := license_client.NewLicenseClient(clientConfig)


	err = client.DeactivateLicense(context.Background(), core_request.LicenseRequest{
			Product: productCode,
			Auth: auth.FromKey(productAuth.LicenseKey),
		})
		
	fmt.Println(err)

	return nil


}

