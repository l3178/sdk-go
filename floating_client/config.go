package floating_client

import (
	"gitlab.com/l3178/sdk-go/core/configuration"
	"gitlab.com/l3178/sdk-go/license_file"
	"time"
)

const (
	apiPrefix = "/api/v4/floating"
)

type FloatingClientConfiguration struct {
	*configuration.CoreConfiguration
	CryptoProvider license_file.CryptoProvider
	DataLocation license_file.DataLocation
}

func (config FloatingClientConfiguration) AuthHeader(date time.Time) string {
	return ""
}

func (config FloatingClientConfiguration) UrlPrefix() string {
	return config.CoreConfiguration.UrlPrefix()
}

func NewFloatingClientConfiguration(url string) FloatingClientConfiguration {
	config := configuration.NewClientConfig(url, apiPrefix)

	return FloatingClientConfiguration{
		CoreConfiguration: config,
		CryptoProvider: license_file.DefaultCryptoProvider{},
		DataLocation: license_file.DefaultDataLocation{},
	}
}

func NewFloatingClientConfigurationCustom(cryptoProvider license_file.CryptoProvider, dataLocation license_file.DataLocation, url string) FloatingClientConfiguration {
	config := configuration.NewClientConfig(url, apiPrefix)

	return FloatingClientConfiguration{
		CoreConfiguration: config,
		CryptoProvider: cryptoProvider,
		DataLocation: dataLocation,
	}
}