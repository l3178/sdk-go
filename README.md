This is the official Go SDK for LicenseSpring.

## Installation

`go get -u gitlab.com/l3178/sdk-go/...`

#### Specific versions

If you want to install a specific version, see the list of versions [here](https://gitlab.com/l3178/sdk-go/-/tags).

## How to use

This SDK contains 3 different modules:

- [License client](license_client)
- [Floating client](floating_client)
- [Management client](management_client)

## Samples and Usage Instructions

The `samples/` directory contains examples demonstrating both **online** and **offline activations and deactivation** using the SDK.

### Setup Instructions

Before running the samples, you need to provide **API credentials and product information** in the `samples/config.yaml` file.

#### Edit the Config File:
Open the `samples/config.yaml` file and provide the following information:

- **API Key**: Your API key.  
- **Shared Key**: A shared key.  
- **Product Details**: Add product-specific information (e.g., license key or username/password for user-based licenses).
- **verbose**: If true, enables detailed logging for debugging purposes.
- **verify_signature**: If true, verifies the server’s signature to ensure message integrity.

#### Run the Samples:
Once the config file is ready, **call the sample from `main.go`** by passing the **product name as an argument**:

