module gitlab.com/l3178/sdk-go

go 1.18

require (
	github.com/denisbrodbeck/machineid v1.0.1
	github.com/go-resty/resty/v2 v2.7.0
	github.com/google/go-cmp v0.5.2
	github.com/google/uuid v1.3.0
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.6.1
	golang.org/x/crypto v0.28.0
	gopkg.in/yaml.v2 v2.4.0
)

require gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fatemebagherii/orderedmap v1.0.0
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
)
