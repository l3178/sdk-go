package license

import (
	"gitlab.com/l3178/sdk-go/license_client"
)

const (
	apiKey     = ""
	sharedKey  = ""
	product    = "test"
	baseUrl    = "https://api-staging.licensespring.com"
	hardwareID = "SDK-TEST"
)

func Setup() license_client.LicenseClient {
	config := license_client.NewLicenseClientConfiguration(apiKey, sharedKey, product)
	config.BaseUrl = baseUrl
	config.HardwareId = hardwareID
	return license_client.NewLicenseClient(config)
}
